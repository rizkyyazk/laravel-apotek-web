@extends('layout.master')
@section('judul')
Tambah Data Penjualan
@endsection

@section('content')
    <form action="/penjualan" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama Users</label>
            <select name="id_user" id="id_user" class="form-control">
                <option value="">--Pilih User--</option>
                @forelse($users as $key => $usr)
                    <option value="{{$usr->id}}">{{$usr->username}}</option>
                @empty
                <option value="">--Data Kosong--</option>
                @endforelse
            </select>
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Nama Obat</label>
            <select name="id_obat" id="obt" class="form-control">
                <option value="">--Pilih Obat--</option>
                @forelse($obat as $key => $obt)
                    <option value="{{$obt->id}}" data="{{$obt->harga}}">{{$obt->nama}}</option>
                @empty
                <option value="">--Data Kosong--</option>
                @endforelse
            </select>
            <input type="hidden" class="form-control" id="nama" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Nama Pembeli</label>
            <input type="text" class="form-control" id="supplier" name="pembeli">
        </div>
        @error('pembeli')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Harga</label>
            <input type="number" readonly class="form-control" id="Harga" name="harga">
        </div>
        @error('harga')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Quantity</label>
            <input type="number" class="form-control" id="qty" name="quantity">
        </div>
        @error('quantity')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="">Total</label>
            <input type="number" class="form-control" id="totalHarga" name="totalHarga">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
<script>
    $(document).on('change','#obt', function(){
        var selectedHarga = $('#obt option:selected').attr('data');
        $('#Harga').val(selectedHarga);
    });

    $(document).on('keyup','#qty', function(){
        var harga = $('#Harga').val();
        var qty = $(this).val();

        var totalHarga = parseInt(harga)*parseInt(qty);
        if(totalHarga == 0){
            isNan(totalHarga);
        }
        $('#totalHarga').val(totalHarga);
    });
</script>
@stack('script')