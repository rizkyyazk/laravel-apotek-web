@extends('layout.master')
@section('judul')
Halaman Detail Penjualan
@endsection

@section('content')
<table class="table">
    <thead class="thead-light">
        <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Quantity</th>
            <th>Total</th>
        </tr>
    </thead>
   
    <tbody>
        <tr>
            <td>{{$penjualan->nama}}</td>
            <td>{{$penjualan->harga}}</td>
            <td>{{$penjualan->quantity}}</td>
            <td>{{$penjualan->total}}</td>
        </tr>
    </tbody>
</table>
@endsection