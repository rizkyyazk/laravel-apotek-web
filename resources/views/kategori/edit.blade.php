@extends('layout.master')
@section('judul')
Update Data Kategori
@endsection()
@section('content')
    <form action="/kategori/{{$kategori->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" value="{{$kategori->nama}}" name="nama" class="form-control" id="" placeholder="Masukkan nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection()