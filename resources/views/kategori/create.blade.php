@extends('layout.master')
@section('judul')
Create Data Kategori
@endsection()
@section('content')
    <form action="/kategori" method="POST">
        @csrf
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="nama" class="form-control" id="" placeholder="Masukkan nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection()