@extends('layout.master')
@section('judul')
Halaman Data kategori
@endsection()
@section('content')
    <a href="/kategori/create" class="btn btn-primary mb-3">Tambah Kategori</a>  
    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">id</th>
            <th scope="col">Nama</th>
            <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($kategori as $key => $item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    <form action="/kategori/{{$item->id}}" method="post">
                        @csrf
                        @method('delete')
                        <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning">Edit</a> 
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @empty
            <h1 class="dt">Data Kosong</h1>
            <style>
            .dt{
                text-align:center;
            }
            .table{
                display:none;
            }
        </style>
            @endforelse
            </tr>
        </tbody>
    </table>
@endsection()