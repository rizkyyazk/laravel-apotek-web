@extends('layout.master')
@section('judul')
Halaman Detail Pembelian
@endsection

@section('content')
<table class="table">
    <thead class="thead-light">
        <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Quantity</th>
            <th>Total</th>
        </tr>
    </thead>
   
    <tbody>
        <tr>
            <td>{{$pembelian->nama}}</td>
            <td>{{$pembelian->harga}}</td>
            <td>{{$pembelian->quantity}}</td>
            <td>{{$pembelian->total}}</td>
        </tr>
    </tbody>
</table>
@endsection