@extends('layout.master')
@section('judul')
Halaman Data Pembelian
@endsection()
@section('content')
<a href="/pembelian/create" class="btn btn-primary my-3"> Tambah Pembelian Baru <a>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Id_User</th>
      <th scope="col">Id_Obat</th>
      <th scope="col">Nama Supplier</th>
      <th scope="col">Harga</th>
      <th scope="col">Quantity</th>
      <th scope="col">Total</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($pembelian as $key => $item)
        <tr>
            <td>{{$key +1}}</td>
            <td>{{$item->id_user}}</td>
            <td>{{$item->id_obat}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->harga}}</td>
            <td>{{$item->quantity}}</td>
            <td>{{$item->total}}</td>
            <td>
            <a href="/pembelian/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="javascript:void(0)" id="del" data-id_pemb="{{$item->id}}" class="btn btn-danger btn-sm" value="Delete">Delete</ 
            </td>
        </tr>       
    @empty
        <h1 class="dt">Data Kosong</h1>
        <style>
          .dt{
            text-align:center;
          }
          .table{
            display:none;
          }
        </style>
    @endforelse
  </tbody>
</table>
@endsection()
<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
  <script>
    $(document).on('click','#del',function(e){  
      var id_pemb = $(this).data('id_pemb');
        e.preventDefault(e);
        Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          location.assign("/pembelian/delete/" + id_pemb);
        }
      })
    });
  </script>
@stack('script')
