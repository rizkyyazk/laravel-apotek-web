@extends('layout.master')
@section('judul')
Create Data Obat
@endsection()
@section('content')
    <form action="/obat" method="POST">
        @csrf
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="nama" class="form-control" id="" placeholder="Masukkan nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label for="">Harga</label>
            <input type="text" name="harga" class="form-control" id="" placeholder="Masukkan harga">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label for="">Stock</label>
            <input type="text" name="stock" class="form-control" id="" placeholder="Masukkan stock">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label for="">Id_Kategori</label>
            <input type="text" name="id_kategori" class="form-control" id="" placeholder="Masukkan id_kategori">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection()