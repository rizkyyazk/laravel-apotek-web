@extends('layout.master')
@section('judul')
Halaman Data Obat
@endsection()
@section('content')
    <a href="/obat/create" class="btn btn-primary mb-3">Tambah Obat</a>  
    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">id</th>
            <th scope="col">Nama</th>
            <th scope="col">Harga</th>
            <th scope="col">Stock</th>
            <th scope="col">Id_Kategori</th>
            <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($obat as $key => $item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->harga}}</td>
                <td>{{$item->stock}}</td>
                <td>{{$item->id_kategori}}</td>
                <td>
                    <form action="/obat/{{$item->id}}" method="post">
                        @csrf
                        @method('delete')
                        <a href="/obat/{{$item->id}}/edit" class="btn btn-warning">Edit</a> 
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @empty
                <h1 class="dt">Data Kosong</h1>
                <style>
                .dt{
                    text-align:center;
                }
                .table{
                    display:none;
                }
                </style>
            @endforelse
            </tr>
        </tbody>
    </table>
@endsection()