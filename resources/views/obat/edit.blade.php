@extends('layout.master')
@section('judul')
Update Data Obat
@endsection()
@section('content')
    <form action="/obat/{{$obat->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" value="{{$obat->nama}}" name="nama" class="form-control" id="" placeholder="Masukkan nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label for="">Harga</label>
            <input type="text" value="{{$obat->harga}}" name="harga" class="form-control" id="" placeholder="Masukkan harga">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label for="">Stock</label>
            <input type="text" value="{{$obat->stock}}" name="stock" class="form-control" id="" placeholder="Masukkan stock">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label for="">Id_Kategori</label>
            <input type="text" value="{{$obat->id_kategori}}" name="id_kategori" class="form-control" id="" placeholder="Masukkan id_kategori">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection()