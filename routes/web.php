<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::group(['middleware' => ['auth']], function(){
//CRUD kategori
Route::get('/kategori', 'KategoriController@index');
Route::get('/kategori/create', 'KategoriController@create');
Route::post('/kategori', 'KategoriController@store');
Route::get('/kategori/{kategori_id}', 'KategoriController@show');
Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
Route::put('/kategori/{kategori_id}', 'KategoriController@update');
Route::delete('/kategori/{kategori_id}', 'KategoriController@delete');

//CRUD obat
Route::get('/obat', 'ObatController@index');
Route::get('/obat/create', 'ObatController@create');
Route::post('/obat', 'ObatController@store');
Route::get('/obat/{obat_id}', 'ObatController@show');
Route::get('/obat/{obat_id}/edit', 'ObatController@edit');
Route::put('/obat/{obat_id}', 'ObatController@update');
Route::delete('/obat/{obat_id}', 'ObatController@delete');


// Pembelian 
Route::get('/pembelian','PembelianController@index');
Route::get('/pembelian/create','PembelianController@create');
Route::post('/pembelian','PembelianController@store');
Route::get('/pembelian/{pembelian_id}', 'PembelianController@show');
Route::get('/pembelian/delete/{pembelian_id}','PembelianController@delete');

// Penjualan
Route::get('/penjualan','PenjualanController@index');
Route::get('/penjualan/create','PenjualanController@create');
Route::post('/penjualan','PenjualanController@store');
Route::get('/penjualan/{penjualan_id}', 'PenjualanController@show');
Route::get('/penjualan/delete/{penjualan_id}','PenjualanController@delete');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
