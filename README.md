## Final Project

## Kelompok 9

## Anggota Kelompok

-   Rizky
-   Mamad Ahmad
-   Faturahman

## Tema Project

Apotek

## ERD

<img align="center" src="public/img/ERD Final Project.jpg" alt="erd.jpg">

## Link Video

-   Link Demo Aplikasi : [Demo](https://youtu.be/WLfd2619LHg).
-   Link Deploy : [Web](https://laravel.com/docs/container).
