<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    protected $table = 'penjualan';
    protected $fillable = ["id_user", "id_obat", "nama", "harga", "quantity", "total"];
}