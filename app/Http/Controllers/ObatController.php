<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Obat;
use App\Kategori;

class ObatController extends Controller
{
    public function index(){
        //$obat = Obat:all();
        $obat = DB::table('obat')->get();
        return view('obat.index', compact('obat'));
    }

    public function create(){
        return view('obat.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'stock' => 'required',
            'id_kategori' => 'required'
        ],
        [
            'nama.required' => 'Inputan Nama Harus Diisi',
            'harga.required' => 'Inputan Harga Harus Diisi',
            'stock.required' => 'Inputan Stock Harus Diisi',
            'id_kategori.required' => 'Inputan Id_Kategori Harus Diisi'
        ]);

        DB::table('obat')->insert([
            'nama' => $request['nama'],
            'harga' => $request['harga'],
            'stock' => $request['stock'],
            'id_kategori' => $request['id_kategori']
        ]);

        return redirect('/obat');
    }

    public function show($id){
        //$obat = Obat:findOrFail($id);
        $obat = DB::table('obat')->where('id',$id)->first();
        return view('obat.show', compact('obat'));
    }

    public function edit($id){
        //$obat = Obat::findOrFail($id);
        //$kategori = Kategori::all();

        $obat = DB::table('obat')->where('id',$id)->first();
        $kategori = DB::table('kategori')->where('id',$id)->first();
        return view('obat.edit', compact('obat'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'stock' => 'required',
            'id_kategori' => 'required'
        ],
        [
            'nama.required' => 'Inputan Nama Harus Diisi',
            'harga.required' => 'Inputan Harga Harus Diisi',
            'stock.required' => 'Inputan Stock Harus Diisi',
            'id_kategori.required' => 'Inputan Id_Kategori Harus Diisi'
        ]);

        /*
        $obat = Obat::find($id);
        $obat->nama = $request->nama;
        $obat->harga = $request->harga;
        $obat->id_kategori = $request->id_kategori;
        $obat->save();
        */
        
        DB::table('obat')->where('id',$id)
            ->update(
                [
                'nama' => $request['nama'],
                'harga' => $request['harga'],
                'stock' => $request['stock'],
                'id_kategori' => $request['id_kategori']
                ]
            );
        
        return redirect('/obat');
    }

    public function delete($id)
    {
        //$obat = Obat::find($id);
        //$obat->delete();
        DB::table('obat')->where('id', '=', $id)->delete();
        return redirect('/obat');
    }
}
