<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    public function index(){
        $kategori = DB::table('kategori')->get();
        return view('kategori.index', compact('kategori'));
    }

    public function create(){
        return view('kategori.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required'
        ],
        [
            'nama.required' => 'Inputan Nama Harus Diisi'
        ]);

        DB::table('kategori')->insert([
            'nama' => $request['nama']
        ]);

        return redirect('/kategori');
    }

    public function show($id){
        $kategori = DB::table('kategori')->where('id',$id)->first();
        return view('kategori.show', compact('kategori'));
    }

    public function edit($id){
        $kategori = DB::table('kategori')->where('id',$id)->first();
        return view('kategori.edit', compact('kategori'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required'
        ],
        [
            'nama.required' => 'Inputan Nama Harus Diisi'
        ]);

        DB::table('kategori')->where('id',$id)
            ->update(
                [
                'nama' => $request['nama']
                ]
            );
        return redirect('/kategori');
    }

    public function delete($id)
    {
        DB::table('kategori')->where('id', '=', $id)->delete();

        return redirect('/kategori');
    }
}
