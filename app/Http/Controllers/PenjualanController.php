<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PenjualanController extends Controller
{
    public function index()
    {
        $penjualan = DB::table('penjualan')->get();
        return view('penjualan.index', compact('penjualan'));
    }
    
    public function create()
    {
        $obat = DB::table('obat')->get();
        $users = DB::table('users')->get();
        return view('penjualan.create',compact('obat'), compact('users'));
    }

    public function store(Request $request){
        $obat = DB::table('obat')->where('id',$request->id_obat)->first();

        $request->validate([
            'id_user' => 'required',
            'id_obat' => 'required',
            'pembeli' => 'required',
            'harga' => 'required',
            'quantity' => 'required',
            'totalHarga' => 'required'
        ],
        [
            'id_user.required'        => 'User harus dipilih', 
            'id_obat.required' => 'Obat harus diisi',
            'pembeli.required' => 'Nama Pembeli Harus Diisi',
            'harga.required' => 'Harga Harus Diisi',
            'quantity.required' => 'Quantity Harus Diisi',
            'totalHarga.required' => 'Total Harga tidak boleh 0'
        ]);

        DB::table('penjualan')->insert([
            'id_user' => $request['id_user'],
            'id_obat' => $request['id_obat'],
            'nama' => $request['pembeli'],
            'harga' => $request['harga'],
            'quantity' => $request['quantity'],
            'total' => $request['totalHarga']
        ]);
        if($obat){
            DB::table('obat')->where('id', $obat->id)->update([
                'stock' => $obat->stock - $request['quantity']
            ]);
        }
        return redirect('/penjualan');
    }

    public function show($id){
        $penjualan = DB::table('penjualan')->where('id',$id)->first();
        return view('penjualan.show', compact('penjualan'));
    }
    

    public function delete($id_penj){
        $penjualan = DB::table('penjualan')->where('id',$id_penj)->first();
        $obat = DB::table('obat')->where('id',$penjualan->id_obat)->first();

        if($obat){
            DB::table('obat')->where('id',$obat->id)->update([
                'stock' => $obat->stock + $penjualan->quantity
            ]);
        }

        DB::table('penjualan')->where('id', $id_penj)->delete();

        return redirect('/penjualan');
    }
}   