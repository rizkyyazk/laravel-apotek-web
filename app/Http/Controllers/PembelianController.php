<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PembelianController extends Controller
{
    public function index()
    {
        $pembelian = DB::table('pembelian')->get();
        return view('pembelian.index', compact('pembelian'));
    }
    
    public function create()
    {
        $obat = DB::table('obat')->get();
        $users = DB::table('users')->get();
        return view('pembelian.create',compact('obat'), compact('users'));
    }

    public function store(Request $request){
        $obat = DB::table('obat')->where('id',$request->id_obat)->first();

        $request->validate([
            'id_user' => 'required',
            'id_obat' => 'required',
            'supplier' => 'required',
            'harga' => 'required',
            'quantity' => 'required',
            'totalHarga' => 'required'
        ],
        [
            'id_user.required'        => 'User harus dipilih', 
            'id_obat.required' => 'Obat harus diisi',
            'supplier.required' => 'Nama Supplier Harus Diisi',
            'harga.required' => 'Harga Harus Diisi',
            'quantity.required' => 'Quantity Harus Diisi',
            'totalHarga.required' => 'Total Harga tidak boleh 0'
        ]);

        DB::table('pembelian')->insert([
            'id_user' => $request['id_user'],
            'id_obat' => $request['id_obat'],
            'nama' => $request['supplier'],
            'harga' => $request['harga'],
            'quantity' => $request['quantity'],
            'total' => $request['totalHarga']
        ]);
        if($obat){
            DB::table('obat')->where('id', $obat->id)->update([
                'stock' => $obat->stock + $request['quantity']
            ]);
        }
        return redirect('/pembelian');
    }

    public function show($id){
        $pembelian = DB::table('pembelian')->where('id',$id)->first();
        return view('pembelian.show', compact('pembelian'));
    }

    public function delete($id_pemb){
        $pembelian = DB::table('pembelian')->where('id',$id_pemb)->first();
        $obat = DB::table('obat')->where('id',$pembelian->id_obat)->first();

        if($obat){
            DB::table('obat')->where('id',$obat->id)->update([
                'stock' => $obat->stock - $pembelian->quantity
            ]);
        }

        DB::table('pembelian')->where('id', $id_pemb)->delete();

        return redirect('/pembelian');
    }
}   